<?php
/**
 * Plugin Name: Count of subscribers Mail Chimp
 * Description: Count of subscribers Mail Chimp.
 * Version: 0.1
 */


function devise_mc_sub_count() {
	include 'Mailchimp.php';
	$last_run_log     = '/logs/lastrun.log';
	$subfile          = '/logs/subcount.log';
	$last_run         = file_get_contents( plugin_dir_path( __FILE__ ) . $last_run_log );
	$subscriber_count = '';

	if ( time() - $last_run >= 86400 ) {

		$mail_chimp = new MailChimp( '27d602826331950b44d3a8f5bde8e593-us17' );
		$arg        = '';
		$mc         = $mail_chimp->call( 'lists/list', $arg );
		$subscriber_count .= $mc['data'][0]['stats']['member_count'];
		file_put_contents( plugin_dir_path( __FILE__ ) . $last_run_log, time() );
		file_put_contents( plugin_dir_path( __FILE__ ) . $subfile, $subscriber_count );

	} else {

		$subscriber_count .= file_get_contents( plugin_dir_path( __FILE__ ) . $subfile );

	}

	return number_format( $subscriber_count );

}

add_shortcode( 'mc-subscribers', 'devise_mc_sub_count' );
add_filter( 'widget_text', 'do_shortcode' );
